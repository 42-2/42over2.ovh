<?php
session_start();
require_once(dirname(__FILE__)."/account/simpleusers/su.inc.php");
$SimpleUsers = new SimpleUsers();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, inital-scale=1, shrink-to-fit=no">
        <title>Contact us | CatCatch, a game by 42/2!</title>
        <?php include('partials/stylesheets.html'); ?>
    </head>
    <body>
        <?php
            include('partials/javascripts.html');
            include('partials/menu.php');
        ?>
        <main role="main" class="container">
            <h1 class="mt-6 text-center">Contact us</h1>
        </main>
        <br />
        <div class="container">
            <form>
                <div class="form-group">
                    <label for="contactFormName">Name</label>
                    <input type="text" class="form-control" id="contactFormName" placeholder="John Doe">
                </div>
                <div class="form-group">
                    <label for="contactFormEmail">Adresse mail</label>
                    <input type="email" class="form-control" id="contactFormEmail" placeholder="john.doe@stark.space">
                </div>
                <div class="form-group">
                    <label for="contactFormObject">Objet</label>
                    <input type="text" class="form-control" id="contactFormObject" placeholder="You guys rock!">
                </div>
                <div class="form-group">
                    <label for="contactFormMessage">Message</label>
                    <textarea class="form-control" id="contactFormMessage" rows="3" placeholder="Write here!"></textarea>
                </div>
                <button type="submit" class="btn btn-primary" onclick="sendMail(); return false">Send</button>
            </form>
        </div>

        <!-- Email JavaScript -->
        <script type="text/javascript">
            function sendMail() {
                let name = encodeURI(document.getElementById("contactFormName").value);
                let email = encodeURI(document.getElementById("contactFormEmail").value);
                let object = encodeURI(document.getElementById("contactFormObject").value);
                let message = encodeURI(document.getElementById("contactFormMessage").value);
                let corps = "Nom : " + name + "<br />Email : " + email + "<br />Message :<br />" + message;
                let link = "mailto:hello@42over2.ovh"
                    + "?cc="
                    + "&subject=[CatCatch][FormulaireSite] " + object
                    + "&body=" + corps
                ;
                window.location.href = link;
            }
        </script>
        <br />
        <?php include('partials/footer.html'); ?>
    </body>
</html>
<!--{# TODO: Now that we use PHP, think of something else to send mails #}-->