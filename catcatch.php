<?php
session_start();
require_once(dirname(__FILE__)."/account/simpleusers/su.inc.php");
$SimpleUsers = new SimpleUsers();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, inital-scale=1, shrink-to-fit=no">
        <title>CatCatch | 42/2</title>
        <?php include('partials/stylesheets.html'); ?>
    </head>
    <body>
        <?php
            include('partials/javascripts.html');
            include('partials/menu.php');
        ?>
        <!--TODO ADD TRAILER-->
        <div class="container"><img class="img-fluid" src="/assets/img/misc/Multijoueur.png" alt="42/2 members"></div>
        <br />
        <main role="main" class="container">
            <h1 class="mt-6 text-center">The Game</h1>
        </main>
        <br />
        <div class="container">
            <p class="text-justify">
                CatCatch is a multiplayer video game in the third person view. It will consist in a chase game, implying two
                types of players: the chasers and the runners. In order to win, the chasers will have to catch the highest
                number of runners. On their side, the runners will have to survive for the longest time, in the best case
                until the end of the time limit. The characters used by the players will be cats, modelized in a
                modelling-clay-like style. A variety of different cats will be available for the players to choose, each
                with different particular capacities that will allow them to better chase, or better run away. The idea is
                that each chaser cat is inspired from a teacher of EPITA and each runner cat is inspired from a student from
                the 2022 EPITA Strasbourg class.
            </p>
            <p class="text-justify">
                Speed boni, or mali to launch on opponents will randomly appear on the map. The chasers will have a limited
                time to catch and eliminate all the runners, and the runners will have to run away from the chasers until
                the time limit is reached. In order to catch a runner, a chaser will have to be at a close range from him
                and push a button to tackle him. The cats’ special capacities will help them, either to run or to catch:
                teleporting, camouflage, go faster, or make opponent go slower. . . The capacities will be usable on the
                cats of the same type, to handicap them and have them making a less good score. Boni and mali will allow
                players to turn the situation over to their advantage. If all the runners are caught before the end of the
                time, players will be ranked as following: first the chasers, ranked according to the number of runners they
                caught, then the runners, ranked according to the time they survived, and then the chasers having caught no
                runner. If the time limit is reached, the players will be ranked as following: first the remaining runners,
                then the runners who have been caught, ranked according to their surviving time, and then the chasers,
                ranked according on the number of runners they have caught.
            </p>
        </div>
        <main role="main" class="container">
            <h1 class="mt-6 text-center">Gallery</h1>
        </main>
        <br />

        <link rel="stylesheet" type="text/css" href="/assets/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/slick/slick-theme.css"/>

<!--        {#    TODO: replace partner and partners by something that actually makes sense (don't forget the CSS and script at the end of this file too #}-->
        <div class="partners">
            <?php
            $directory = 'assets/img/gallery-img/';
            $scanned_directory = array_slice(scandir($directory), 2);
            shuffle($scanned_directory);
            foreach ($scanned_directory as $slide)
            {
                echo '<div class="partner">';
                echo '<img class="partner" src="' . $directory . $slide . '" alt="CatchCatch screenshot">';
                echo '</div>';
            }
            ?>
        </div>
        <br />
        <main role="main" class="container">
            <h1 class="mt-6 text-center">Download</h1>
        </main>
        <br />
        <div class="container">
            <div class="row">

                <div class="col-sm-2  d-flex align-items-stretch">
                    <div class="card">
                        <img class="img-fluid card-img-top" src="/assets/builds/icons/windows-64.png" alt="Win64">
                        <div class="card-body">
                            <h5 class="card-title">Windows 64-bits</h5>
                            <h6 class="card-subtitle mb-2 text-muted">v2.0</h6>
                            <a href="https://gitlab.com/42-2/CatCatch/CatCatch-executables/raw/master/final-defense/CatCatch-installer-win-x86_64-v3.0.zip" class="btn btn-primary" download>Installer</a><br /><br />
                            <a href="https://gitlab.com/42-2/CatCatch/CatCatch-executables/raw/master/final-defense/CatCatch-win-x86_64-v3.0.zip" class="btn btn-primary" download>Executable</a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2   d-flex align-items-stretch">
                    <div class="card">
                        <img class="img-fluid card-img-top" src="/assets/builds/icons/windows-32.png" alt="Win32">
                        <div class="card-body">
                            <h5 class="card-title">Windows 32-bits</h5>
                            <h6 class="card-subtitle mb-2 text-muted">v2.0</h6>
                            <a href="https://gitlab.com/42-2/CatCatch/CatCatch-executables/raw/master/final-defense/CatCatch-installer-win-x86-v3.0.zip" class="btn btn-primary" download>Installer</a><br /><br />
                            <a href="https://gitlab.com/42-2/CatCatch/CatCatch-executables/raw/master/final-defense/CatCatch-win-x86-v3.0.zip" class="btn btn-primary" download>Executable</a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2 d-flex align-items-stretch">
                    <div class="card">
                        <img class="img-fluid card-img-top" src="/assets/builds/icons/linux-64.png" alt="Linux64">
                        <div class="card-body">
                            <h5 class="card-title">Linux 64-bits</h5>
                            <h6 class="card-subtitle mb-2 text-muted">v2.0</h6>
                            <a href="https://gitlab.com/42-2/CatCatch/CatCatch-executables/raw/master/final-defense/CatCatch-linux-x86_64-v3.0.zip" class="btn btn-primary" download>Executable</a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2 d-flex align-items-stretch">
                    <div class="card">
                        <img class="img-fluid card-img-top" src="/assets/builds/icons/linux-32.png" alt="Linux32">
                        <div class="card-body">
                            <h5 class="card-title">Linux 32-bits</h5>
                            <h6 class="card-subtitle mb-2 text-muted">v2.0</h6>
                            <a href="https://gitlab.com/42-2/CatCatch/CatCatch-executables/raw/master/final-defense/CatCatch-linux-x86-v3.0.zip" class="btn btn-primary" download>Executable</a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2 d-flex align-items-stretch">
                    <div class="card">
                        <img class="img-fluid card-img-top" src="/assets/builds/icons/macos.png" alt="MacOS">
                        <div class="card-body">
                            <h5 class="card-title">MacOS</h5>
                            <h6 class="card-subtitle mb-2 text-muted">v2.0</h6>
                            <a href="https://gitlab.com/42-2/CatCatch/CatCatch-executables/raw/master/final-defense/CatCatch-macos-v3.0.zip" class="btn btn-primary" download>Executable</a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2 d-flex align-items-stretch">
                    <div class="card">
                        <img class="img-fluid card-img-top" src="/assets/builds/icons/html5.png" alt="WebGL">
                        <div class="card-body">
                            <h5 class="card-title">WebGL</h5>
                            <h6 class="card-subtitle mb-2 text-muted">v2.0</h6>
                            <p class="card-text">You can try our game online!!!</p>
                            <a href="/play-online/" class="btn btn-primary">Play now!</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <br />

        <div class="container">
            <ul>
                <li>
                    <i class="fa fa-file-alt"></i> User manual <a href="https://gitlab.com/42-2/CatCatch/CatCatch-executables/raw/master/final-defense/CatCatch-User-Manual-v3.0.pdf" target="_blank" download>here</a> at all times.
                </li>
                <li>
                    <i class="fa fa-code"></i> Source code is available <a href="https://gitlab.com/42-2/CatCatch/CatCatch" target="_blank">here</a> at all times.
                </li>
                <li>
                    <i class="fa fa-file-alt"></i> Dossiers, such as specifications, defense reports and plans, are available <a href="https://gitlab.com/42-2/CatCatch/CatCatch-docs" target="_blank">here</a> at all times.
                </li>
                <li>
                    <i class="fa fa-globe"></i> Source code for this website is available <a href="https://gitlab.com/42-2/CatCatch/CatCatch-site" target="_blank">here</a> at all times.
                </li>
            </ul>
        </div>
        <br/>
        <div class="container">
            <h3>Timeline of the CatCatch project</h3>
            <ul class="timeline">
                <li>
                    <div class="timeline-badge success"><i class="fa fa-thumbs-up"></i></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4 class="timeline-title">Final Defense</h4>
                            <p>
                                <small class="text-muted"><i class="fa fa-clock"></i> June 10<sup>th</sup> 2018</small>
                            </p>
                        </div>
                        <div class="timeline-body">
                            <p>
                                Source code and executables used for the defense are available for download <a href="https://gitlab.com/42-2/CatCatch/CatCatch/tags/Final-Defense" target="_blank">here</a>.<br/>
                                Defense plan is available for download <a href="https://gitlab.com/42-2/CatCatch/CatCatch-docs/tags/v3.1" target="_blank">here</a>.<br/>
                            </p>
                        </div>
                    </div>
                </li>

                <li class="timeline-inverted">
                    <div class="timeline-badge success"><i class="fa fa-file-alt"></i></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4 class="timeline-title">Project Report</h4>
                            <p>
                                <small class="text-muted"><i class="fa fa-clock"></i> May 25<sup>th</sup> 2018</small>
                            </p>
                        </div>
                        <div class="timeline-body">
                            <p>
                                Project report is available for download <a href="https://gitlab.com/42-2/CatCatch/CatCatch-docs/tags/v3.0" target="_blank">here</a>.<br/>
                            </p>
                        </div>
                    </div>
                </li>

                <li>
                    <div class="timeline-badge primary"><i class="fa fa-check"></i></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4 class="timeline-title">Second Defense</h4>
                            <p>
                                <small class="text-muted"><i class="fa fa-clock"></i> May 4<sup>th</sup> (be with you) 2018</small>
                            </p>
                        </div>
                        <div class="timeline-body">
                            <p>
                                Source code and executables used for the defense are available for download <a href="https://gitlab.com/42-2/CatCatch/CatCatch/tags/Second-Defense-Build" target="_blank">here</a>.<br/>
                                Updated specifications, defense plan and defense report are available for download <a href="https://gitlab.com/42-2/CatCatch/CatCatch-docs/tags/v2.0" target="_blank">here</a>.<br/>
                            </p>
                        </div>
                    </div>
                </li>

                <li class="timeline-inverted">
                    <div class="timeline-badge danger"><i class="fa fa-thumbs-down"></i></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4 class="timeline-title">First Defense</h4>
                            <p>
                                <small class="text-muted"><i class="fa fa-clock"></i> March 14<sup>th</sup> 2018</small>
                            </p>
                        </div>
                        <div class="timeline-body">
                            <p>
                                Source code and executables used for the defense are available for download <a href="https://gitlab.com/42-2/CatCatch/CatCatch/tags/v0.1" target="_blank">here</a>.<br/>
                                Updated specifications, defense plan and defense report are available for download <a href="https://gitlab.com/42-2/CatCatch/CatCatch-docs/tags/v1.0" target="_blank">here</a>.<br/>
                            </p>
                        </div>
                    </div>
                </li>

                <li>
                    <div class="timeline-badge info"><i class="fa fa-file-alt"></i></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4 class="timeline-title">Specifications</h4>
                            <p>
                                <small class="text-muted"><i class="fa fa-clock"></i> January 19<sup>th</sup> 2018</small>
                            </p>
                        </div>
                        <div class="timeline-body">
                            <p>Available for download <a href="https://gitlab.com/42-2/CatCatch/CatCatch-docs/tags/v0.5" target="_blank">here</a>.</p>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <br />
        <?php include('partials/footer.html'); ?>
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script type="text/javascript" src="/assets/slick/slick.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.partners').slick({
                    infinite: true,
                    variableWidth: true,
                    autoplay: true,
                    pauseOnFocus: false,
                    pauseOnHover: false,
                });
            });
        </script>
    </body>
</html>