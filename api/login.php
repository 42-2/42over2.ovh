<?php
session_start();
require_once(dirname(__FILE__)."/../account/simpleusers/su.inc.php");

$SimpleUsers = new SimpleUsers();


header("Content-Type:application/json");

if(!empty($_POST['username']) && !empty($_POST['password']))
{
    $username=$_POST['username'];
    $password=$_POST['password'];

    $res = $SimpleUsers->loginUser($username, $password);

    if(!$res)
    {
        response(200,"Credentials not valid",NULL);
    }
    else
    {
        response(200,"Credentials correct", $username);
    }

}
else
{
    response(400,"Invalid Request",NULL);
}

function response($status,$status_message,$data)
{
    header("HTTP/1.1 ".$status);

    $response['status']=$status;
    $response['status_message']=$status_message;
    $response['data']=$data;

    $json_response = json_encode($response);
    echo $json_response;
}