<?php
session_start();
require_once(dirname(__FILE__)."/../account/simpleusers/su.inc.php");

$SimpleUsers = new SimpleUsers();


header("Content-Type:application/json");

if(empty($_POST['username']) || empty($_POST['mode']) || empty($_POST['score']) || empty($_POST['won']))
{
    $username=$_POST['username'];
    $mode=$_POST['mode'];
    $score=$_POST['score'];
    $won=$_POST['won'];

    $res = saveScores($username, $mode, $score, $won);

    if (!$res)
    {
        response(200,"Not Saved",NULL);
    }
    else
    {
        response(200,"Saved", $username);
    }
}
else
{
    response(400,"Invalid Request",NULL);
}

function response($status,$status_message,$data)
{
    header("HTTP/1.1 ".$status);

    $response['status']=$status;
    $response['status_message']=$status_message;
    $response['data']=$data;

    $json_response = json_encode($response);
    echo $json_response;
}

function saveScores($username, $mode, $score, $won)
{
    $SimpleUsers = new SimpleUsers();
    $users = $SimpleUsers->getUsers();
    $user = null;
    foreach ($users as $us){
        if ($us["uUsername"] == $username){
            $user = $us;
            break;
        }
    }
    if ($mode == "2"){
        $old_multi_nb_games = ($SimpleUsers->getInfo("multi_nb_games", $user["userId"]) == "" ? 0 : intval($SimpleUsers->getInfo("multi_nb_games", $user["userId"])));
        $old_multi_score_tot = ($SimpleUsers->getInfo("multi_score_tot", $user["userId"]) == "" ? 0 : intval($SimpleUsers->getInfo("multi_score_tot", $user["userId"])));
        $old_multi_score_won = ($SimpleUsers->getInfo("multi_score_won", $user["userId"]) == "" ? 0 : intval($SimpleUsers->getInfo("multi_score_won", $user["userId"])));
        $multi_nb_game = $old_multi_nb_games + 1;
        $multi_score_tot = $old_multi_score_tot + intval($score);
        $multi_score_won = $old_multi_score_won + ($won == "True" ? 1 : 0);

        $SimpleUsers->setInfo("multi_nb_games", strval($multi_nb_game), $user["userId"]);
        $SimpleUsers->setInfo("multi_score_tot", strval($multi_score_tot), $user["userId"]);
        $SimpleUsers->setInfo("multi_score_won", strval($multi_score_won), $user["userId"]);
        return TRUE;

    }
    elseif ($mode == "3"){
        $old_br_nb_games = ($SimpleUsers->getInfo("br_nb_games") == "" ? 0 : intval($SimpleUsers->getInfo("br_nb_games", $user["userId"])));
        $old_br_score_tot = ($SimpleUsers->getInfo("br_score_tot") == "" ? 0 : intval($SimpleUsers->getInfo("br_score_tot", $user["userId"])));
        $old_br_score_won = ($SimpleUsers->getInfo("br_score_won") == "" ? 0 : intval($SimpleUsers->getInfo("br_score_won", $user["userId"])));
        $br_nb_game = $old_br_nb_games + 1;
        $br_score_tot = $old_br_score_tot + intval($score);
        $br_score_won = $old_br_score_won + ($won == "True" ? 1 : 0);

        $SimpleUsers->setInfo("br_nb_games", strval($br_nb_game));
        $SimpleUsers->setInfo("br_score_tot", strval($br_score_tot));
        $SimpleUsers->setInfo("br_score_won", strval($br_score_won));
        return TRUE;

    }

    return FALSE;
}