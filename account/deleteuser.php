<?php

	/**
	* Make sure you started your'e sessions!
	* You need to include su.inc.php to make SimpleUsers Work
	* After that, create an instance of SimpleUsers and your'e all set!
	*/

	session_start();
	require_once(dirname(__FILE__)."/simpleusers/su.inc.php");

	$SimpleUsers = new SimpleUsers();

	// This is a simple way of validating if a user is logged in or not.
	// If the user is logged in, the value is (bool)true - otherwise (bool)false.
	if( !$SimpleUsers->logged_in )
	{
		header("Location: login.php");
		exit;
	}

	// If the user is logged in, we can safely proceed.
	$userId = $_GET["userId"];

	//Delete the user (plain and simple)
	$SimpleUsers->deleteUser($userId);

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, inital-scale=1, shrink-to-fit=no">
    <title>Delete your account | 42/2</title>
    <?php include('../partials/stylesheets.html'); ?>
</head>
<body>
<?php
include('../partials/javascripts.html');
include('../partials/menu.php');
?>
<main role="main" class="container">
    <h1 class="mt-6 text-center">Delete your account</h1>
</main>
<br />
<div class="container">
    <p class="text-justify">
        Your account has been successfully deleted
    </p>
</div>
<br />
<?php include('../partials/footer.html'); ?>
</body>
</html>