<?php

	/**
	* Make sure you started your'e sessions!
	* You need to include su.inc.php to make SimpleUsers Work
	* After that, create an instance of SimpleUsers and your'e all set!
	*/

	session_start();
	require_once(dirname(__FILE__)."/simpleusers/su.inc.php");

	$SimpleUsers = new SimpleUsers();

	// This is a simple way of validating if a user is logged in or not.
	// If the user is logged in, the value is (bool)true - otherwise (bool)false.
	if( !$SimpleUsers->logged_in )
	{
		header("Location: login.php");
		exit;
	}

	// If the user is logged in, we can safely proceed.


	$userId = $_GET["userId"];

	$user = $SimpleUsers->getSingleUser($userId);
	if( !$user )
		die("The user could not be found...");


	// Validation of input
	if( isset($_POST["password"]) )
	{
		if( empty($_POST["password"]) )
			$error = "You have to choose a password";
    else
    {
    	// Input validation is ok, set the password and then redirect
			$SimpleUsers->setPassword($_POST["password"], $user["userId"]);
			header("Location: users.php");
			exit;
		}

	} // Validation end

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, inital-scale=1, shrink-to-fit=no">
    <title>The Game | 42/2</title>
    <?php include('../partials/stylesheets.html'); ?>
</head>
<body>
<?php
include('../partials/javascripts.html');
include('../partials/menu.php');
?>
<main role="main" class="container">
    <h1 class="mt-6 text-center">Change password for user <?php echo $user["uUsername"]; ?></h1>
</main>
<br />
<div class="container">

		<?php if( isset($error) ): ?>
		<p>
			<?php echo $error; ?>
		</p>
		<?php endif; ?>

		<form method="post" action="">

			<p>
				<label for="password">New password:</label><br />
				<input type="password" name="password" id="password" />
			</p>

			<p>
				<input type="submit" name="submit" value="Save" />
			</p>

		</form>

</div>
<br />
<?php include('../partials/footer.html'); ?>
</body>
</html>
