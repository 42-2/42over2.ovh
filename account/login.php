<?php

	/**
	* Make sure you started your sessions!
	* You need to include su.inc.php to make SimpleUsers Work
	* After that, create an instance of SimpleUsers and you're all set!
	*/

	session_start();
	require_once(dirname(__FILE__)."/simpleusers/su.inc.php");

	$SimpleUsers = new SimpleUsers();

	// Login from post data
	if( isset($_POST["username"]) )
	{

		// Attempt to login the user - if credentials are valid, it returns the users id, otherwise (bool)false.
		$res = $SimpleUsers->loginUser($_POST["username"], $_POST["password"]);
		if(!$res)
			$error = "You supplied the wrong credentials.";
		else
		{
				header("Location: index.php");
				exit;
		}

	} // Validation end

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, inital-scale=1, shrink-to-fit=no">
    <title>Login | CatCatch, a game by 42/2!</title>
    <?php include('../partials/stylesheets.html'); ?>
</head>
<body>
<?php
include('../partials/javascripts.html');
include('../partials/menu.php');
?>
<main role="main" class="container">
    <h1 class="mt-6 text-center">Login | 42/2</h1>
</main>
<br />
<div class="container">
		<?php if( isset($error) ): ?>
		<p>
			<?php echo $error; ?>
		</p>
		<?php endif; ?>

		<form method="post" action="">
			<p>
				<label for="username">Username</label><br />
				<input type="text" name="username" id="username" />
			</p>

			<p>
				<label for="password">Password</label><br />
				<input name="password" id="password" type="password"/>
			</p>

			<p>
				<input type="submit" name="submit" value="Login" />
			</p>

		</form>
</div>
<br />
<?php include('../partials/footer.html'); ?>
</body>
</html>