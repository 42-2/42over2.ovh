<?php

	/**
	* Make sure you started your'e sessions!
	* You need to include su.inc.php to make SimpleUsers Work
	* After that, create an instance of SimpleUsers and your'e all set!
	*/

	session_start();
	require_once(dirname(__FILE__)."/simpleusers/su.inc.php");

	$SimpleUsers = new SimpleUsers();

	// This simply logs out the current user
	$SimpleUsers->logoutUser();

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, inital-scale=1, shrink-to-fit=no">
    <title>Logout | 42/2</title>
    <?php include('../partials/stylesheets.html'); ?>
</head>
<body>
<?php
include('../partials/javascripts.html');
include('../partials/menu.php');
?>
<main role="main" class="container">
    <h1 class="mt-6 text-center">Logout</h1>
</main>
<br />
<div class="container">
    <p class="text-justify">
        Your have been successfully logged out.
    </p>
</div>
<br />
<?php include('../partials/footer.html'); ?>
</body>
</html>
