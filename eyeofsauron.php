<?php
session_start();
require_once(dirname(__FILE__)."/account/simpleusers/su.inc.php");
$SimpleUsers = new SimpleUsers();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, inital-scale=1, shrink-to-fit=no">
        <title>Eye of Sauron | 42/2</title>
        <?php include('partials/stylesheets.html'); ?>
    </head>
    <body>
        <?php
            include('partials/javascripts.html');
            include('partials/menu.php');
        ?>
        <main role="main" class="container">
            <h1 class="mt-6 text-center">Eye of Sauron</h1>
        </main>
        <br/>
        <div class="container">
            <p class="text-justify">
                Eye of Sauron is a program that those some OCR (Optical Character Recognition). Basically, you give him
                and image with text in it, and it will hand you back your text. Everything is explained in our GitLab
                repository which you will find below.
            </p>
        </div>

        <div class="container">
            <h3>Timeline of the Eye of Sauron project</h3>
            <ul class="timeline">
                <li>
                    <div class="timeline-badge success"><i class="fa fa-thumbs-up"></i></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4 class="timeline-title">Final Defense</h4>
                            <p>
                                <small class="text-muted"><i class="fa fa-clock"></i> December 5<sup>th</sup> 2018</small>
                            </p>
                        </div>
                        <div class="timeline-body">
                            <p>
                                Source code and executables used for the defense are available for download <a href="https://gitlab.com/42-2/eyeofsauron/tags/Final-Defense" target="_blank">here</a>.<br/>
                                Defense plan is available for download <a href="https://gitlab.com/42-2/CatCatch/CatCatch-docs/tags/v3.1" target="_blank">here</a>.<br/>
                            </p>
                        </div>
                    </div>
                </li>

                <li class="timeline-inverted">
                    <div class="timeline-badge success"><i class="fa fa-file-alt"></i></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4 class="timeline-title">Project Report</h4>
                            <p>
                                <small class="text-muted"><i class="fa fa-clock"></i> December 5<sup>th</sup> 2018</small>
                            </p>
                        </div>
                        <div class="timeline-body">
                            <p>
                                Project report is available for download <a href="https://gitlab.com/42-2/CatCatch/CatCatch-docs/tags/v3.0" target="_blank">here</a>.<br/>
                            </p>
                        </div>
                    </div>
                </li>

                <li>
                    <div class="timeline-badge danger"><i class="fa fa-thumbs-down"></i></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4 class="timeline-title">First Defense</h4>
                            <p>
                                <small class="text-muted"><i class="fa fa-clock"></i> October 24<sup>th</sup> 2018</small>
                            </p>
                        </div>
                        <div class="timeline-body">
                            <p>
                                Source code and executables used for the defense are available for download <a href="https://gitlab.com/42-2/CatCatch/CatCatch/tags/v0.1" target="_blank">here</a>.<br/>
                                Updated specifications, defense plan and defense report are available for download <a href="https://gitlab.com/42-2/CatCatch/CatCatch-docs/tags/v1.0" target="_blank">here</a>.<br/>
                            </p>
                        </div>
                    </div>
                </li>

            </ul>
        </div>
        <br />
        <?php include('partials/footer.html'); ?>
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script type="text/javascript" src="/assets/slick/slick.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.partners').slick({
                    infinite: true,
                    variableWidth: true,
                    autoplay: true,
                    pauseOnFocus: false,
                    pauseOnHover: false,
                });
            });
        </script>
    </body>
</html>