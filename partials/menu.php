<header>
    <!-- NavBar w/ logo -->
    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
        <a class="navbar-brand" href="/">
            <img src="/assets/favicon.png" width="40" height="30" class="d-inline-block align-top" alt="">
            42/2
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
            <ul class="navbar-nav mr-sm-2">
                <li class="nav-item <?php if ($_SERVER['REQUEST_URI'] == '/') echo "active"; ?>">
                    <a class="nav-link" href="/">Home</a>
                </li>
                <li class="nav-item <?php if ($_SERVER['REQUEST_URI'] == '/about-us.php') echo "active"; ?>">
                    <a class="nav-link" href="/about-us.php">About Us</a>
                </li>
                <li class="nav-item <?php if ($_SERVER['REQUEST_URI'] == '/catcatch.php') echo "active"; ?>">
                    <a class="nav-link" href="/catcatch.php">CatCatch</a> <!-- TODO: add logo next to it -->
                </li>
                <li class="nav-item <?php if ($_SERVER['REQUEST_URI'] == '/eyeofsauron.php') echo "active"; ?>">
                    <a class="nav-link" href="/eyeofsauron.php">Eye of Sauron</a> <!-- TODO: add logo next to it -->
                </li>
                <li class="nav-item <?php if ($_SERVER['REQUEST_URI'] == '/contact-us.php') echo "active"; ?>">
                    <a class="nav-link" href="/contact-us.php">Contact</a>
                </li>
                <?php if( $SimpleUsers->logged_in ): ?>
                <li class="dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?php  echo $SimpleUsers->getSingleUser()["uUsername"]; ?>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="/account/">Stats</a>
                        <a class="dropdown-item" href="/account/changepassword.php">Change password</a>
                        <a class="dropdown-item" href="/account/logout.php">Logout</a>
                    </div>
                </li>
                <?php else: ?>
                    <li class="nav-item <?php if ($_SERVER['REQUEST_URI'] == '/account/login.php') echo "active"; ?>">
                        <a class="nav-link" href="/account/login.php">Login</a>
                    </li>
                    <li class="nav-item <?php if ($_SERVER['REQUEST_URI'] == '/account/register.php') echo "active"; ?>">
                        <a class="nav-link" href="/account/register.php">Register</a>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
    </nav>
</header>
<br/>
<br/>
<br/>