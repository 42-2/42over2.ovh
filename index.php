<?php
session_start();
require_once(dirname(__FILE__)."/account/simpleusers/su.inc.php");
$SimpleUsers = new SimpleUsers();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, inital-scale=1, shrink-to-fit=no">
        <title>42/2</title>
        <?php include('partials/stylesheets.html'); ?>
    </head>
    <body>
        <?php
            include('partials/javascripts.html');
            include('partials/menu.php');
        ?>
        <main role="main" class="container">
            <h1 class="mt-6 text-center">42/2, the best team ever!</h1>
        </main>
        <br />
        <div class="container">
            <div class="text-center">
                <h2>Discover our projects now</h2>
                <br/>
                <h4>CatCatch, watch the trailers here</h4>
                <div class="container">
                    <p class="text-justify">
                        Learn more <a href="/catcatch.php">here</a>.
                    </p>
                </div>
                <div class="row">
                    <div class="col-sm-6"><iframe width="560" height="315" src="https://www.youtube.com/embed/G59uuY08oMk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>
                    <div class="col-sm-6"><iframe width="560" height="315" src="https://www.youtube.com/embed/AxK7ZTJnsws" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>
                </div>
                <h4>Eye of Sauron</h4>
                <div class="container">
                    <p class="text-justify">
                        Learn more <a href="/eyeofsauron.php">here</a>.
                    </p>
                </div>
            </div>
        </div>
        <br />
        <?php include('partials/footer.html'); ?>
    </body>
</html>